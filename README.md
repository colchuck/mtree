# MTree Master Key System Chart Creation

Simple chart creation for printing or PDF export (via printing dialog). Written in primarily in JQuery and PHP.

## Details

Not intended to be run publicly, as this software does not include any soft of authentication by itself.
However, you could run without ever saving and no data would be sent back to the server.
Compatible with Chromium-based browsers and Firefox 80+, as many other browsers do not support the proper printing settings.

## Requires

MySQL (or Compatible), PHP 7+, Web Server

## Installation

Move files into web server directory. Import the "charts.sql" file into database of a MySQL compatible server.
Edit the 'config.php' with the credentials and location of the SQL server.
Delete/move the 'charts.sql' file.
Replace the 'res/img/logoFull.png' file with your own logo.

## Printing

Browsers often disable "Print Backgrounds" by default and add margins. Change these settings and set to landscape for optimal results.

## Screenshot

![](screenshot.png)

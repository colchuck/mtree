$(document).ready(function() {
  /*************************GLOBAL VARIABLES**************************/
  window.nextNode = 1
  window.loadedID = 'n'
  window.revisionLoaded = false
  window.undo = []
  window.redo = []
  window.hasChanges = false

  /****************function declairations*********************************/
  function revisionMode(sw) {
    window.revisionLoaded = sw
    window.hasChanges = false;
    if (sw) {
      $("#revInfo").show()
      $(".moveIcon").css({
        "display": "none"
      })
    } else {
      $("#revInfo").hide()
      $("#revisionList").val(null)
      $(".moveIcon").css({
        "display": "block"
      })
    }
    $("#removeChart").prop("disabled", sw)
    $("#saveBtn").prop("disabled", sw)
    $("#loadBtnDiag").prop("disabled", sw)
    $("#menuLoadBtn").prop("disabled", sw)
    $("#sysList").prop("disabled", sw);
    $("#newChart").prop("disabled", sw);
    $("#chartWrap input").prop("disabled", sw);
    $("#info input").prop("disabled", sw)
    $("#revert").prop("disabled", !sw)
    $("#notes").prop("contenteditable", !sw)
    $("#exitRevision").prop("disabled", !sw)
    $("#rightMenu input").prop("disabled", sw)
    $("#zoomLevel").prop("disabled", false) //make sure this is always enabled for navigation
  }

  function loadSystems() {
    s = $("#sysList").val()
    $.getJSON("getList.php", function(data) {
      $("#sysList").empty()
      $.each(data, function(k, v) {
        $("#sysList").append("<option value='" + v['_id'] + "'>" + v["sysName"] + "</option>")
      });
      $("#sysList").val(s)
    });
  }

  $("#doCollapse").change(function() {
    if ($("#doCollapse").is(":checked")) {
      window.collapse = true;
    } else {
      window.collapse = false;
    }
    hrLines()
  });

  window.collapse = false;
  window.collapseChart = function() {
    console.log("Collapse is running")
    //$(".subTree").each(function() {
    //  $(this).css({"margin-right":"5px","margin-left":"5px"})
    //});
    $(".subTree").each(function() {
      //if subtree has siblings and is a parent
      if ($(this).children(".subTree").length > 0 && $(this).siblings(".subTree").length > 0) {

        //right branch collapse from selected parent
        var rightWidthToNextParent = 0; //sum of the widths between this parent node and the next one to the right
        var nextParent = [];
        $(this).nextAll(".subTree").each(function(p, k) {
          if ($(this).children(".subTree").length == 0) {
            rightWidthToNextParent += $(this).width()
          } else {
            nextParent = this;
            return false;
          }
        });

        //if there is no next parent
        if ($(nextParent).length == 0) {
          if (!rightWidthToNextParent == 0) {
            //"Has no prev parent, has nodes next to it"
            if (!($(this).width() / 2) > rightWidthToNextParent) {
              rightWidthToNextParent = 100000
            }
          } else {
            //"Has no prev parent, has no nodes next to it"
            rightWidthToNextParent = -5
          }
        } else {
          if (!rightWidthToNextParent == 0) {
            //"Has prev parent, isn't right next to it"
            //rightWidthToNextParent += parseInt($(nextParent).css("margin-right"))
            rightWidthToNextParent = 1000000
            //rightWidthToNextParent += 10 //margin
          } else {
            //"has prev parent, is right next door"
            rightWidthToNextParent = -5;
          }
        }

        r = -($(this).width() - ($(this).width() / 2 + ($(this).children(".node").width() / 2)) - 5)
        if (rightWidthToNextParent > Math.abs(r)) {
          $(this).css({
            "margin-right": r
          });
        } else {
          $(this).css({
            "margin-right": -rightWidthToNextParent
          });
        }

        //left banch collapse from selected parent
        //console.log("startLEFT")
        var rightWidthToNextParent = 0; //sum of the widths between this parent node and the next one to the right
        var nextParent = [];
        $(this).prevAll(".subTree").each(function(p, k) {
          if ($(this).children(".subTree").length == 0) {
            rightWidthToNextParent += $(this).width()
          } else {
            nextParent = this;
            return false;
          }
        });

        //if there is no next parent
        if ($(nextParent).length == 0) {
          if (!rightWidthToNextParent == 0) {
            //"Has no prev parent, has nodes next to it"
            if (!($(this).width() / 2) > rightWidthToNextParent) {
              rightWidthToNextParent = 100000
            }
          } else {
            //"Has no prev parent, has no nodes next to it"
            rightWidthToNextParent = -5
          }
        } else {
          if (!rightWidthToNextParent == 0) {
            //"Has prev parent, isn't right next to it"
            rightWidthToNextParent += parseInt($(nextParent).css("margin-right"))
            //rightWidthToNextParent += 10 //margin
          } else {
            //"has prev parent, is right next door"
            rightWidthToNextParent = -5;
          }
        }

        //r is the same for left and right branch, we don't need to calc this twice.
        //if the move will overlap, only go the maximum distance before overlap
        if (rightWidthToNextParent > Math.abs(r)) {
          $(this).css({
            "margin-left": r
          });
        } else {
          $(this).css({
            "margin-left": -rightWidthToNextParent
          });
        }

        //make sure large subtrees don't cover up small ones
        $(this).css({
          "z-index": "0"
        })
      } else {
        $(this).css({
          "z-index": "3"
        })
      }
    });
    //center inner nodes attempt
    $(".subTree").each(function() {
      //if is inbetween parents
      if ($(this).children(".subTree").length > 0 && $(this).siblings(".subTree").length > 0) {
        var nextParent = [];
        inbetweenNodes = [];
        $(this).nextAll(".subTree").each(function() {
          if ($(this).children(".subTree").length == 0) {
            inbetweenNodes.push(this)
          } else {
            nextParent = this;
            return false;
          }
        });
        //if there is a next parent and nodes inbetween.
        if ($(nextParent).length != 0 && inbetweenNodes.length != 0) {
          //console.log(this)
          nWidth = parseInt($("#widthLevel").val())

          tWidth = $(this).width() / 2 + $(nextParent).width() / 2 - 10 //for margin
          console.log("thing:" + (nWidth * inbetweenNodes.length + (10 * inbetweenNodes.length)))
          console.log(tWidth)
          if ((nWidth * inbetweenNodes.length + (10 * inbetweenNodes.length)) + nWidth > tWidth) {
            return
          }
          parts = (tWidth) / (inbetweenNodes.length + 1)
          console.log(parts)
          for (i = 0; i < inbetweenNodes.length; i++) {

            if (i == 0) {
              pos = parts - (nWidth + 10)
            } else {
              pos = parts * (i + 1) - (nWidth + 10) * (i) - (nWidth + 10)
            }
            console.log(pos)
            $(inbetweenNodes[i]).css({
              "left": pos
            });
          }
        }
      }
    });
  }

  window.setSortable = function() {
    $(".subTree").each(function() {
      if ($(this).is(':ui-sortable')) {
        $(this).sortable('destroy');
      }
    });
    $(".moveIcon").css({
      "display": "visable"
    })
    $(".node").each(function() {
      if ($(this).siblings(".subTree").length > 0) {
        $(this).parent().sortable({
          items: $(this).siblings(".subTree"),
          handle: '.moveIcon ',
          placeholder: 'sortable-placeholder subTree',
          change: function(event, ui) {
            $(".sortable-placeholder").width($(ui.item).width())
          },
          start: function(event, ui) {
            $(".sortable-placeholder").width($(ui.item).width())
            zoomScale = (parseFloat($("#zoomLevel").val()) / 100)
          },
          sort: function(e, ui) {
            zoomScale = (parseFloat($("#zoomLevel").val()) / 100)
            var newLeft = ui.position.left / zoomScale;
            var newTop = ui.position.top / zoomScale;
            ui.helper.css({
              left: newLeft,
              top: newTop,
            });
          }
        });
      }
    })
    $(".subTree").unbind('sortupdate')
    $(".subTree").on('sortupdate', function() {
      hrLines()
      window.undo.push($("#chartWrap").clone(true, true))
      window.redo = []
    });
  }

  window.loadRevisions = function() {
    selectedR = $("#revisionList").val()
    if (window.revisionLoaded) {
      lid = window.parentID
    } else {
      lid = window.loadedID
    }
    $.getJSON("getRevisionList.php?id=" + lid, function(data) {
      $("#revisionList").empty();
      $.each(data["revisions"], function(key, val) {
        $("#revisionList").append("<option value=" + val["_id"] + ">" + val["tmstamp"] + "</option>")
      });
      $("#revisionList").val(selectedR)
    });
  }

  function addSub(dis, label = "", desc = "", seq = "", parent = false, nid = false, note = "") {

    skip = false
    if (parent === false) {
      skip = true
    }
    if (parent == "r") {
      $("#rLabel").val(label)
      $("#rDesc").val(desc)
      $("#rSmallText").val(note)
      return
    }
    if (skip) {
      window.undo.push($("#chartWrap").clone(true, true))
      window.redo = []
      nid = window.nextNode
      parent = $(this).parent().parent().data('nodeid')
      seq = $(this).parent().parent().siblings(".subTree").length
    }
    var nodeHTML = `<div class='subTree'><div class='node' data-nodeid="` + nid + `" data-parentn="` + parent + `" data-seq="` + seq + `"><div class='vLine'></div><img src='res/img/move.png' class='moveIcon'><img src='res/img/Gear-512.png' class="gears"><input type="text" value="` + note + `" placeholder="Note" class="smallLabel"/><input type='text'
    placeholder='Label' value='` + label + `' class='label'><textarea placeholder='Description' class='desc'>` + desc + `</textarea>
    <div style='height:30px; width:98%;' class="nBtnBox"><input type='button' class='addSub' tabindex="-1" value='New Sub'><input type='button' tabindex="-1" class='del' value='Remove'></div><div class='vLineB'></div></div><br><div class='leftHr'></div><div class='rightHr'></div><br></div>`
    if (skip) {
      $(this).parent().parent().parent(".subTree").append(nodeHTML)
      window.nextNode += 1
      $(".gears").unbind()
      $(".gears").click(openGears)
      $(".addSub").unbind()
      $(".addSub").click(addSub)
      $(".del").unbind()
      $(".del").click(delSub)
      widthLevels(false);
      nodeHeightLevels(false);
      $("#heightLevel").trigger("input")
      nodeStyles()
      hrLines()
      setSortable()
    } else {
      $(".node[data-nodeid=" + parent + "]").parent(".subTree").append(nodeHTML)
    }

  }

  function delSub() {
    window.undo.push($("#chartWrap").clone(true, true))
    window.redo = []
    $(this).parent().parent().parent(".subTree").remove()
    hrLines()
    setSortable()
  }

  window.undoFunc = function() {
    if (window.undo.length == 0) return;
    var last = window.undo.pop()
    window.redo.push($("#chartWrap").clone(true, true))
    $("#chartWrap").replaceWith(last)
    hrLines()
    $(".gears").unbind()
    $(".gears").click(openGears)
    $(".addSub").unbind()
    $(".addSub").click(addSub)
    $(".del").unbind()
    $(".del").click(delSub)
    $("#zoomLevel").trigger("input")
    $("#widthLevel").trigger("input")
    $("#heightLevel").trigger("input")
    $("#nodeHeightLevel").trigger("input")
    setSortable()
    nodeStyles()
  }

  window.redoFunc = function() {
    if (window.redo.length == 0) return;
    red = window.redo.pop()
    window.undo.push($("#chartWrap").clone(true, true))
    $("#chartWrap").replaceWith(red)
    hrLines()
    $(".gears").unbind()
    $(".gears").click(openGears)
    $(".addSub").unbind()
    $(".addSub").click(addSub)
    $(".del").unbind()
    $(".del").click(delSub)
    $("#widthLevel").trigger("input")
    $("#heightLevel").trigger("input")
    $("#nodeHeightLevel").trigger("input")
    $("#zoomLevel").trigger("input")
    nodeStyles()
  }

  function openGears() {
    var offest = $(this).offset();
    var height = $(this).height();
    $("#gearsDiag").dialog('option', 'position', {
      my: "left top",
      at: "left bottom",
      of: $(this)
    });
    $('#gearsDiag').dialog("option")
    $("#gearsDiag").dialog("open");
  };

  window.exportChart = function() {
    var fname = $("#sysName").val();
    if (fname == null || fname == "") fname = "New_System";
    fname = fname.trim()
    fname = fname.replace(/ /g, "_")
    fname = fname.replace(/\W/g, '')
    createOutData();
    d = JSON.stringify(window.outData)
    var blob = new Blob([d], {
      type: "text/plain;charset=utf-8"
    });
    saveAs(blob, fname + ".mkc")
  }

  window.changeDetector = function() {
    $("input[type=color], input[type=text]").change(function() {
      window.hasChanges = true;
    });
    $("#chartWrap input[type=button]").click(function() {
      window.hasChanges = true;
    });
  }

  changeDetector()

  window.createOutData = function() {
    outData = {}
    outData["notes"] = $("#notes").html()
    outData["sysName"] = $("#sysName").val()
    if (outData["sysName"].trim() == "") outData["sysName"] = "Unnamed Chart"
    outData["zoomLevel"] = $("#zoomLevel").val()
    outData["widthLevel"] = $("#widthLevel").val()
    outData["heightLevel"] = $("#heightLevel").val()
    outData["nodeHeightLevel"] = $("#nodeHeightLevel").val()
    outData["color1"] = $("#color1").val()
    outData["color2"] = $("#color2").val()
    outData["descColor"] = $("#descColor").val()
    outData["noteColor"] = $("#noteColor").val()
    outData["labelColor"] = $("#labelColor").val()
    outData["borderColor"] = $("#borderColor").val()
    outData["shadow"] = $("#shadow").is(":checked")
    outData["nextNode"] = window.nextNode
    outData["nodes"] = {}
    counter = 0;
    $(".node").each(function(key, val) {
      nid = $(val).data("nodeid")
      outData["nodes"][counter] = {
        "nodeid": $(val).data("nodeid"),
        "seq": $(val).data("seq"),
        "parentn": $(val).data("parentn"),
        "desc": $(val).children(".desc").val(),
        "label": $(val).children(".label").val(),
        "note": $(val).children(".smallLabel").val()
      }
      counter++;
    });
    window.outData = outData
  }

  window.onbeforeprint = function() {
    pos = $("#zoomLevel").val()
    pos = parseInt(pos) / 100;
    $("#chartWrap").css({
      "transform": 'scale(' + pos + ') translate(-50%,-50%)'
    });
  }

  window.onafterprint = function() {
    $("#zoomLevel").trigger("input")
  }

  window.saveChart = function(saveID) {
    if (saveID == null) {
      saveID = window.loadedID
    }
    window.createOutData()
    console.log(saveID)
    $.post("save.php?id=" + saveID, window.outData, function(data) {
      data = JSON.parse(data)
      if (data["newId"] != "s") {
        window.loadedID = data["newId"]
      }
      $("#lastSavedDate").text(data["tmstamp"])
      loadRevisions()
      loadSystems()
      window.hasChanges = false;
    });
  }

  function nodeStyles() {
    $(".node").css({
      "background": "linear-gradient(330deg, " + $("#color2").val() + " 0%, " + $("#color1").val() + " 100%)"
    })
    $(".desc").css({
      "color": $("#descColor").val()
    })
    $(".smallLabel").css({
      "color": $("#noteColor").val()
    })
    $(".label").css({
      "color": $("#labelColor").val()
    })
    $(".node").css({
      "border": "1px solid" + $("#borderColor").val()
    })
    if ($("#shadow").is(":checked")) {
      $(".node").css({
        "box-shadow": "5px 5px 10px lightgrey"
      })
    } else {
      $(".node").css({
        "box-shadow": "none"
      })
    }
  }

  window.hrLines = function(olens = "") {
    console.log(olens)
    $(".rightHr").width(0)
    $(".leftHr").width(0)
    $(".leftHr").css({
      "margin-left": "0px"
    })
    lens = ""
    if (window.collapse) {
      collapseChart()
    }
    $(".node").each(function() {
      //if it has siblings
      if (!window.collapse) {
        $(this).parent().css({
          "left": ""
        })
        if ($(this).parent(".subTree").siblings(".subTree").length == 0) {
          $(this).parent(".subTree").css({
            "margin-right": "0px",
            "margin-left": "0px"
          })
        } else {
          $(this).parent(".subTree").css({
            "margin-right": "5px",
            "margin-left": "5px"
          })
        }
      }
      //if it has children
      if ($(this).siblings(".subTree").length > 0) {
        $(this).siblings(".rightHr").width(0)
        $(this).siblings(".leftHr").width(0)
        scale = (parseFloat($("#zoomLevel").val()) / 100)
        distanceL = ($(this).offset().left / scale + $(this).width() / 2) - ($(this).siblings(".subTree").first().offset().left / scale + $(this).siblings(".subTree").first().width() / 2)
        distanceR = ($(this).offset().left / scale + $(this).width() / 2) - ($(this).siblings(".subTree").last().offset().left / scale + $(this).siblings(".subTree").last().width() / 2)
        ofs = Math.ceil($(this).siblings(".subTree").first().width() / 2)
        distanceL = Math.abs(Math.floor(distanceL))
        distanceR = Math.abs(Math.floor(distanceR))
        if ($(this).siblings(".subTree").length == 1) {
          distanceL = 0
          distanceR = 0
        }
        //sometimes we get a weird 1 value because of rounding I assume.
        if (distanceL == 1) {
          distanceL = 0;
        }
        if (distanceR == 1) {
          distanceR = 0;
        }
        lens += distanceL.toString() + distanceR.toString()
        $(this).siblings(".leftHr").width(distanceL)
        $(this).siblings(".leftHr").css({
          "margin-left": ofs + 3
        })
        $(this).siblings(".rightHr").width(distanceR)
        $(this).children(".vLineB").show()
        //$(this).siblings(".rightHr").css({"margin-left":ofs})
      } else {
        $(this).children(".vLineB").hide()
      }
    });
    if (olens !== false) {
      if (lens != olens) {
        //sections are still moving, we need to recaculate until they stop
        window.hrLines(lens)
      }
    }
    $("#zoomLevel").trigger("input")
  }

  window.resetPage = function() {
    window.undo = []
    window.redo = []
    $("#sysName").val("")
    $("#notes").empty().html("<b>Address:</b>&nbsp;<br><b>Keyway:</b>&nbsp;<br><b>Card:</b>&nbsp;</b><br><b>Date:</b>&nbsp;")
    $("#zoomLevel").val(100)
    $("#widthLevel").val(150)
    $("#color1").val("#FFFFFF")
    $("#color2").val("#d4d4d4")
    $("#descColor").val("#000000")
    $("#noteColor").val("#000000")
    $("#borderColor").val("#D3D3D3")
    $("#labelColor").val("#000000")
    $("#shadow").prop("checked", true)
    $("#nodeHeightLevel").val(100)
    nodeHeightLevels(false)
    $("#rSmallText").val("")
    $("#heightLevel").val(0)
    widthLevels(false)
    $("#heightLevel").trigger("input")
    $("#zoomLevel").change()
    $(".node").first().siblings(".subTree").remove()
    $(".label").val("")
    $(".desc").val("")
    $(".rightHr").width(0)
    $(".leftHr").width(0)
    nodeStyles()
    hrLines(false)
    $("#zoomLevel").trigger("input")
  }

  function loadData() {
    window.resetPage()
    $("#notes").html(outData["notes"])
    $("#sysName").val(outData["sysName"])
    $("#zoomLevel").val(outData["zoomLevel"])
    $("#widthLevel").val(outData["widthLevel"])
    $("#heightLevel").val(outData["heightLevel"])
    $("#nodeHeightLevel").val(outData["nodeHeightLevel"])
    $("#color1").val(outData["color1"])
    $("#color2").val(outData["color2"])
    $("#descColor").val(outData["descColor"])
    $("#noteColor").val(outData["noteColor"])
    $("#labelColor").val(outData["labelColor"])
    $("#borderColor").val(outData["borderColor"])
    $("#shadow").prop("checked", (outData["shadow"] == "true"))
    $("#sysName").keyup()
    $("#notes").keyup()
    window.nextNode = parseInt(outData["nextNode"])
    loadedIds = []
    loadLater = []
    $.each(window.outData["nodes"], function(k, v) {
      loadedIds.push(v["nodeid"])
      if (v["parentn"] != "r" && !loadedIds.includes(v["parentn"])) {
        //parent hasn't been loaded yet, so crap.
        loadLater.push(v)
      }
      addSub(null, v["label"], v["desc"], v["seq"], v["parentn"], v["nodeid"], v["note"])
    });
    window.undo = []
    window.redo = []
    window.nextNode += 1
    $(".gears").unbind()
    $(".gears").click(openGears)
    $(".addSub").unbind()
    $(".addSub").click(addSub)
    $(".del").unbind()
    $(".del").click(delSub)
    $("#widthLevel").trigger("input")
    $("#heightLevel").trigger("input")
    $("#nodeHeightLevel").trigger("input")
    nodeStyles()
    hrLines()
    setSortable()
    changeDetector()
  }

  /*****************Intial Loading functions*****************************/
  loadSystems();
  resetPage();

  /**********************Event Handlers*******************************/
  $(".addSub").click(addSub);
  $(".del").click(delSub);
  $(".gears").click(openGears);

  $("#chartWrap").bind('mousewheel', function(e) {
    if (e.originalEvent.wheelDelta / 120 > 0) {
      $("#zoomLevel").val(parseInt($("#zoomLevel").val()) + 10)
      $("#zoomLevel").trigger("input")
    } else {
      $("#zoomLevel").val($("#zoomLevel").val() - 10)
      $("#zoomLevel").trigger("input")
    }
  });

  /*************************dialog decliariations************************/
  $("#loadDiag").dialog({
    modal: true,
    draggable: false,
    title: "Import/Export File"
  });
  $("#gearsDiag").dialog({
    title: "Key Settings",
    draggable: false,
    width: 170
  });
  $("#confirmDiag").dialog({
    modal: true,
    draggable: false,
    title: "Confirmation Required"
  });
  $("#gearsDiag").dialog("close")
  $("#confirmDiag").dialog("close");
  $("#loadDiag").dialog("close");

  $("#confirmDiag").on("dialogopen", function() {
    $(".ui-dialog").siblings().not(".ui-dialog").css({
      "filter": "blur(4px)"
    })
  })
  $("#confirmDiag").on("dialogclose", function() {
    $(".ui-dialog").siblings().not(".ui-dialog").css({
      "filter": "unset"
    })
  })
  $("#loadDiag").on("dialogopen", function() {
    $(".ui-dialog").siblings().not(".ui-dialog").css({
      "filter": "blur(4px)"
    })
  })
  $("#loadDiag").on("dialogclose", function() {
    $(".ui-dialog").siblings().not(".ui-dialog").css({
      "filter": "unset"
    })
  })

  /**********************undo/redo handlers******************************/
  $("#undoBtn").click(window.undoFunc);

  $("#redoBtn").click(window.redoFunc);

  /*********************left menu handlers******************************/
  $("#uploadBtn").click(function() {
    var files = document.getElementById('selectFiles').files;
    if (files.length <= 0) {
      return false;
    }
    var fr = new FileReader();
    fr.onload = function(e) {
      var result = JSON.parse(e.target.result);
      window.outData = result
      window.loadedID = 'n'
      $("#lastSavedDate").text("Never - Import")
      loadData()
    }
    fr.readAsText(files.item(0));
    $("#loadDiag").dialog("close")
    $("#selectFiles").val("")
    $("#selectFiles").change()
  });

  $("#exportBtn").click(exportChart);
  $("#cancelLoad").click(function() {
    $("#loadDiag").dialog("close")
  })

  $("#removeRevision").click(function() {
    $("#confirmMsg").text("Are you sure you want to remove this revision?")
    $("#confirmDiag").dialog("open");
    $("#confirmBtn").click(function() {
      $.post("delete.php?id=" + $("#revisionList").val(), function() {
        window.loadedID = window.parentID
        loadRevisions()
        loadSystems()
        $("#cancelBtn").off()
        $("#confirmBtn").off()
        $("#confirmDiag").dialog("close")
        //reload the head if we delete a revision
        $.getJSON("load.php?id=" + window.parentID, function(data) {
          window.outData = data["chartData"]
          window.loadedID = $("#sysList").val()
          $("#lastSavedDate").text(data["tmstamp"])
          loadData()
          revisionMode(false)
          loadRevisions()
        });
      });
    });

    $("#cancelBtn").click(function() {
      $("#cancelBtn").off()
      $("#confirmBtn").off()
      $("#confirmDiag").dialog("close")
    });
  });

  $("#removeChart").click(function() {
    $("#confirmMsg").text("Are you sure you want to remove this chart and all of it's revisions? This action cannot be undone.")
    $("#confirmDiag").dialog("open");
    //check if loaded id is the one being deleted

    $("#confirmBtn").click(function() {
      $.post("delete.php?id=" + $("#sysList").val(), function() {
        if (parseInt($("#sysList").val()) == window.loadedID) {
          $("#newChart").click()
        }
        loadSystems()
        loadRevisions()
        $("#cancelBtn").off()
        $("#confirmBtn").off()
        $("#confirmDiag").dialog("close")
      });
    });

    $("#cancelBtn").click(function() {
      $("#cancelBtn").off()
      $("#confirmBtn").off()
      $("#confirmDiag").dialog("close")
    });
  });

  $("#selectFiles").change(function() {
    if (document.getElementById("selectFiles").files.length > 0) {
      $("#uploadBtn").attr("disabled", false);
    } else {
      $("#uploadBtn").attr("disabled", true);
    }
  });

  $("#newChart").click(function() {
    window.resetPage()
    window.nextNode = 1
    window.loadedID = 'n'
    window.parentID = null
  });

  $("#sysList").dblclick(function() {
    $("#menuLoadBtn").click()
  });

  $("#menuLoadBtn").click(function() {
    if (window.hasChanges) {
      $("#confirmMsg").text("Are you sure? Any unsaved changes will be lost.")

      $("#confirmDiag").dialog("open");
      $("#confirmBtn").click(function() {
        $.getJSON("load.php?id=" + $("#sysList").val(), function(data) {
          window.outData = data["chartData"]
          $("#lastSavedDate").text(data["tmstamp"])
          window.loadedID = $("#sysList").val()
          loadData()
          revisionMode(false)
          loadRevisions()
          window.hasChanges = false
          changeDetector()
        });
        $("#cancelBtn").off()
        $("#confirmBtn").off()
        $("#confirmDiag").dialog("close")
      });

      $("#cancelBtn").click(function() {
        $("#cancelBtn").off()
        $("#confirmBtn").off()
        $("#confirmDiag").dialog("close")
      });

    } else {
      $.getJSON("load.php?id=" + $("#sysList").val(), function(data) {
        window.outData = data["chartData"]
        $("#lastSavedDate").text(data["tmstamp"])
        window.loadedID = $("#sysList").val()
        loadData()
        revisionMode(false)
        loadRevisions()
      });
    }
  });

  $("#closeSideMenuBtn").click(function() {
    $("#sideMenu").animate({
      width: '0'
    }, function() {
      $("#sideMenu").css({
        display: "none"
      });
    });
    $(document).off("mouseup")
  });

  $("#sideMenuPin").click(function() {
    if ($("#sideMenuPin").data("pinned") == "1") {
      $("#sideMenuPin").data("pinned", "0")
      $("#sideMenuPin").css({
        "opacity": ".6"
      })
      $(document).mouseup(function(e) {
        var container = $("#sideMenu");
        // If the target of the click isn't the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          $("#sideMenu").animate({
            width: '0'
          }, function() {
            $("#sideMenu").css({
              display: "none"
            });
          });
          $(document).off("mouseup")
        }
      });
    } else {
      $("#sideMenuPin").data("pinned", "1")
      $("#sideMenuPin").css({
        "opacity": "1"
      })
      $(document).off("mouseup")
    }
  });

  $("#sideMenuLaunch").click(function() {
    $("#sideMenu").css({
      display: "block"
    });
    $("#sideMenu").animate({
      width: '300'
    });
    $(document).mouseup(function(e) {
      var container = $("#sideMenu");
      var overflay = $(".ui-widget-overlay, .ui-dialog")
      var diags = $(".ui-dialog").find("*");
      // If the target of the click isn't the container
      if (!container.is(e.target) && container.has(e.target).length === 0 && !overflay.is(e.target) && !diags.is(e.target)) {
        $("#sideMenu").animate({
          width: '0'
        }, function() {
          $("#sideMenu").css({
            display: "none"
          });
        });
        $(document).off("mouseup")
      }
    });
  });
  $("#loadRevision").click(function() {
    $.getJSON("load.php?id=" + $("#revisionList").val(), function(data) {
      window.outData = data["chartData"]
      $("#lastSavedDate").text(data["tmstamp"])
      if (!window.revisionLoaded) {
        window.parentID = window.loadedID
      }
      window.loadedID = $("#revisionList").val()
      loadData()
      revisionMode(true)
      loadRevisions()
    });
  });

  $("#revisionList").dblclick(function() {
    $("#loadRevision").click();
  });

  $("#loadBtnDiag").click(function() {
    $("#loadDiag").dialog("open")
  });

  $("#saveBtnF").click(function(){
    $("#saveBtnF").fadeOut().delay(1000).fadeIn()
    $("#checkMarkF").fadeIn().delay(1000).fadeOut()
  });

  $("#saveBtn, #saveBtnF").click(function() {
    saveChart()
  });

  /*****************upper menu handlers**********************************/
  $("#notes").keyup(function() {
    $("#notesOut").html($(this).html())
  });

  $("#sysName").keyup(function() {
    $("#sysNameOut").text($(this).val())
  });

  $("#zoomLevel").on("input", function(event, ui) {
    pos = $("#zoomLevel").val()
    pos = parseInt(pos) / 100;
    $("#chartWrap").css({
      "transform": 'scale(' + pos + ')'
    });

    $("#whitSpcRm").css({
      width: $("#chartWrap")[0].getBoundingClientRect().width,
      height: $("#chartWrap")[0].getBoundingClientRect().height,
    });

    $("#chartWrap").css({
      "margin-left": ($("#chartWrap")[0].getBoundingClientRect().width - $("#chartWrap").width()) / 2
    });
  });

  //runs only when the slider stops, running on input is too expensive
  $("#zoomLevel").on("change", function() {
    hrLines(false);
  });

  $("#exitRevision").click(function() {
    $.getJSON("load.php?id=" + window.parentID, function(data) {
      window.outData = data["chartData"]
      window.loadedID = $("#sysList").val()
      $("#lastSavedDate").text(data["tmstamp"])
      loadData()
      revisionMode(false)
      loadRevisions()
    });
  });

  $("#revert").click(function() {
    //save this revision as the head - we need the head system's id
    $("#confirmMsg").text("Are you sure you want to revert the chart back to this revision? The current revision will be saved as a revision.")

    $("#confirmDiag").dialog("open");
    $("#confirmBtn").click(function() {
      saveChart(window.parentID)
      $.getJSON("load.php?id=" + window.loadedID, function(data) {
        window.outData = data["chartData"]
        window.loadedID = window.parentID
        $("#lastSavedDate").text(data["tmstamp"])
        loadData()
        revisionMode(false)
        loadRevisions()
        $("#cancelBtn").off()
        $("#confirmBtn").off()
        $("#confirmDiag").dialog("close")
      });
    });

    $("#cancelBtn").click(function() {
      $("#cancelBtn").off()
      $("#confirmBtn").off()
      $("#confirmDiag").dialog("close")
    });

  });

  /*************************right style menu handlers**********************/
  $("#rightMenuBtn").click(function() {
    $("#rightMenu").css({
      display: "block"
    });
    $("#rightMenu").animate({
      width: '300'
    });
    $(document).mouseup(function(e) {
      var container = $("#rightMenu");
      // If the target of the click isn't the container
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#rightMenu").animate({
          width: '0'
        }, function() {
          $("#rightMenu").css({
            display: "none"
          });
        });
        $(document).off("mouseup")
      }
    });
  });

  $("#closeRightMenuBtn").click(function() {
    $("#rightMenu").animate({
      width: '0'
    }, function() {
      $("#rightMenu").css({
        display: "none"
      });
    });
    $(document).off("mouseup")
  });

  $("#heightLevel").on("input", function(event, ui) {
    pos = $("#heightLevel").val()
    $(".rightHr").css({
      "margin-bottom": parseInt(pos) + 11
    })
    $(".leftHr").css({
      "margin-bottom": parseInt(pos) + 11
    })
    $(".rightHr").css({
      "margin-top": parseInt(pos)
    })
    $(".leftHr").css({
      "margin-top": parseInt(pos)
    })
    $(".vLineB").css({
      "height": parseInt(pos) + 20,
      "bottom": -(parseInt(pos) + 20)
    })
    $(".vLine").css({
      "height": parseInt(pos) + 20,
      "top": -(parseInt(pos) + 20),
      "margin-bottom": -(parseInt(pos))
    })
  });

  function nodeHeightLevels(fixLines = true) {
    pos = $("#nodeHeightLevel").val()
    $(".node").css({
      "height": parseInt(pos)
    })
    if (fixLines) {
      hrLines(false)
    }
  }

  $("#nodeHeightLevel").on("input", function(event, ui) {
    nodeHeightLevels(true)
  });

  function widthLevels(fixLines = true) {
    pos = $("#widthLevel").val()
    $(".node").css({
      "width": pos
    })
    if (fixLines) {
      hrLines(false)
    }
  }

  $("#widthLevel").on("input", function(event, ui) {
    widthLevels(true)
  });

  $("#color1").change(nodeStyles);
  $("#color2").change(nodeStyles);
  $("#noteColor").change(nodeStyles);
  $("#descColor").change(nodeStyles);
  $("#labelColor").change(nodeStyles);
  $("#borderColor").change(nodeStyles);
  $("#shadow").change(nodeStyles)

});

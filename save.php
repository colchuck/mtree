<?php
/*********************
Handles saving of charts to database
*********************/

require_once 'config.php';

//db connection
try {
    $dbh = new PDO($dsn, $config['dbUser'], $config['dbPass']);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}

$saveTime;
$newId;

//if it is a new chart
if($_GET["id"]=="n")
{

  //insert new chart into database
  $stmt = $dbh->prepare('INSERT INTO `charts` (`sysName`,`json`,`tmstamp`) VALUES (:sName,:sJSON,CURRENT_TIMESTAMP)');
  $stmt->bindParam(':sName', $_POST['sysName'], PDO::PARAM_STR);
  $je = json_encode($_POST);
  $stmt->bindParam(':sJSON', $je,PDO::PARAM_STR);
  $stmt->execute();

  //get last inserted id
  $stmt = $dbh->prepare('SELECT LAST_INSERT_ID()');
  $stmt->execute();
  $row = $stmt->fetchAll();
  $newId = $row[0][0];

  //get last saved date
  $getTime=$dbh->prepare("SELECT DATE_FORMAT(tmstamp,'%m/%d/%Y %T') as tmstamp  FROM charts WHERE _id = :idNo");
  $getTime->bindParam(':idNo', $newId, PDO::PARAM_INT);
  $getTime->execute();
  $saveTime = $getTime->fetch()["tmstamp"];

//if it is an existing chart
} else {

  //get currently stored data
  $stmt = $dbh->prepare('SELECT `sysName`,`json`,`tmstamp` FROM charts WHERE _id = :idNo' );
  $stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  $stmt->execute();
  $old = $stmt->fetch();

  //make sure exists
  if($stmt->rowCount() !== 1)
  {
    echo "NO_MATCH";
    die();
  }
  //copy that row to a revision row
  $stmt = $dbh->prepare('INSERT INTO `charts` (`sysName`,`json`,`tmstamp`,`parent_id`) VALUES (:sName,:sJSON,:sTimeStamp,:idNo)');
  $stmt->bindParam(':idNo', $_GET['id'],PDO::PARAM_INT);
  $stmt->bindParam(':sName', $old['sysName'], PDO::PARAM_STR);
  $stmt->bindParam(':sJSON', $old['json'], PDO::PARAM_STR);
  $stmt->bindParam(':sTimeStamp', $old['tmstamp'], PDO::PARAM_STR);
  $stmt->execute();

  //add new data to parent id
  $stmt = $dbh->prepare('UPDATE `charts` SET `sysName` = :sName, `json` = :sJSON, `tmstamp` = CURRENT_TIMESTAMP WHERE _id = :idNo');
  $stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  $stmt->bindParam(':sName', $_POST['sysName'], PDO::PARAM_STR);
  $je = json_encode($_POST);
  $stmt->bindParam(':sJSON', $je,PDO::PARAM_STR);
  $stmt->execute();

  //get the saved date (now)
  $getTime = $dbh->prepare("SELECT DATE_FORMAT(tmstamp,'%m/%d/%Y %T') as tmstamp  FROM charts WHERE _id = :idNo");
  $getTime->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  $getTime->execute();
  $saveTime = $getTime->fetch()["tmstamp"];
  $newId = "s";
}

//return json data
echo json_encode(["newId"=>$newId,"tmstamp"=>$saveTime]);

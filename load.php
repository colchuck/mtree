<?php
/****************************
Loads the master key system data via the id
*******************************/
require_once 'config.php';

try {
    $dbh = new PDO($dsn, $config['dbUser'], $config['dbPass']);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}

//load the requested id from db
$stmt = $dbh->prepare("SELECT json, DATE_FORMAT(tmstamp,'%m/%d/%Y %T') as tmstamp FROM charts WHERE _id = :idNo");
$stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
if (1 !== $stmt->rowCount()) {
     echo 'NO_MATCH';
     die();
}
$row = $stmt->fetch();
$result["tmstamp"] = $row["tmstamp"];
$result["chartData"] = json_decode($row['json']);

//return as json object
echo json_encode($result);

<?php

//get the list of charts from the database

require_once 'config.php';

//db connection
try {
    $dbh = new PDO($dsn, $config['dbUser'], $config['dbPass']);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}

//select only current revisions
$stmt = $dbh->prepare("SELECT sysName,_id FROM charts WHERE parent_id is NULL ORDER BY sysName");
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

//return as json
echo json_encode($rows);
?>

<?php

//gets the revisions for a seleted chart

require_once 'config.php';

//db connection
try {
    $dbh = new PDO($dsn, $config['dbUser'], $config['dbPass']);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}

//select only revisions for desired id
$stmt = $dbh->prepare("SELECT _id, DATE_FORMAT(tmstamp,'%m/%d/%Y %T') as tmstamp FROM charts WHERE parent_id = :idNo ORDER BY tmstamp DESC");
$stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
$stmt->execute();
$revisions = $stmt->fetchAll(PDO::FETCH_ASSOC);
$result = [];
$result["revisions"] = $revisions;

//return as json object
echo json_encode($result);

 ?>

<?php

//deletes a record from the system

require_once 'config.php';

//db connection
try {
    $dbh = new PDO($dsn, $config['dbUser'], $config['dbPass']);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}

//check if trying to delete a new chart
if($_GET["id"]=="n")
{
  echo "INVALID_ARG";
} else {
  
  //check id input
  if(!is_numeric($_GET["id"]))
  {
    echo "INVALID_ARG";
    die();
  }

  //check if a valid id and check if revision or head
  $stmt = $dbh->prepare('SELECT parent_id FROM `charts` WHERE _id = :idNo');
  $stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  $stmt->execute();
  if($stmt->rowCount()!= 1)
  {
    echo "INVALID_ID";
    die();
  }
  $row = $stmt->fetchALL(PDO::FETCH_ASSOC);

  //if it is a head revision, delete child revisions
  if(is_null($row[0]["parent_id"])){
    $stmt = $dbh->prepare('DELETE FROM `charts` WHERE _id = :idNo OR parent_id = :idNo');
    $stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  } else {
    $stmt = $dbh->prepare('DELETE FROM `charts` WHERE _id = :idNo');
    $stmt->bindParam(':idNo', $_GET['id'], PDO::PARAM_INT);
  }
  //run delete statement
  $stmt->execute();
}

 ?>
